const express = require('express');
const multer = require('multer');
const upload = multer();

const router = express.Router();
const hezuoCtrl = require('../controllers/jjh-hezuo');

router.get('/hezuo', hezuoCtrl.hezuo);

// router.get('/hezuoFind', hezuoCtrl.hezuoFind);

router.post('/postAddUpload', upload.single('feature'), hezuoCtrl.postAddUpload);


module.exports = router;