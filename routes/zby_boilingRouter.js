// 引入express包
const express = require('express');
// 使用express创建路由
const route = express.Router();
// 引入控制器
const boilingCtrl = require('../controllers/zby_boilingCtrl');

// 1.显示 沸点页面
route.get('/boiling', boilingCtrl.boiling);

// 2.发布内容
route.post('/addBoiling',boilingCtrl.addBoiling);

// 3. 显示发布内容
route.get('/showBoiling',boilingCtrl.showBoiling);

// 4. 删除发布内容
route.get('/delBoiling',boilingCtrl.delBoiling);

// 暴露路由
module.exports = route;