// 引入express包
const express = require('express');
// 使用express创建路由
const route = express.Router();
// 引入multer
const multer = require('multer');
const upload = multer();
// 引入控制器
const shareCtrl = require('../controllers/zby_shareCtrl');

// 1.显示 分享链接页面
route.get('/share-link',shareCtrl.share);

// 2. 上传图片
route.post('/addFile',upload.single('feature'),shareCtrl.addFile);

// 3. 发布数据
route.post('/pubShare',shareCtrl.pubShare);

// 暴露路由
module.exports = route;