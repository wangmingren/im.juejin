const express = require('express');
const router = express.Router();
const activeCtrl = require('../controllers/jjh-active');

router.get('/active',activeCtrl.active);

router.get('/activeFind',activeCtrl.activeFind);

module.exports = router;