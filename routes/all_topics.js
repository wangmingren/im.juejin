const express = require('express');

const router = express.Router();

const topCrl = require('../controllers/all_topics');

router.get('/topics', topCrl.topics);

router.get('/alltopics', topCrl.alltopics);

module.exports = router;