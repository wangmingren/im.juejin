const express = require('express');
const router = express.Router();
const wenjianCtrl = require('../controllers/jjh-wenjian');

router.get('/wenjian', wenjianCtrl.wenjian);

module.exports = router;