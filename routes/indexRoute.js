const express = require('express');
const router = express.Router();
const conn = require('../controllers/indexCtrl')
router.get('/',conn.index);
router.get('/indexShow',conn.indexShow);
router.get('/indexDown',conn.indexDown);
router.get('/indexAdd',conn.indexAdd);
module.exports = router;