const express = require('express');
// 使用express的router来创建路由
const router = express.Router();
// 引入控制器模块
const controller = require('../controllers/ZBX_Contro');
// 配置注册页面显示

// 配置登陆页面显示
router.get('/login', controller.login);
// 配置首页显示
router.get('/zhuce', controller.zhuce);

// 注册
router.post('/userAdd', controller.userAdd);
// 登陆
router.post('/reset', controller.reset);


// router.get('/index',controller.index);
// 配置 

module.exports = router;