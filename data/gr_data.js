const mysql = require('mysql');
const connection = mysql.createConnection({
    // 本地地址
    host: '127.0.0.1',
    // 端口
    port: '3306',
    // 用户名
    user: 'root',
    // 密码
    password: '123456',
    // 数据库名字
    database: 'juejin'
})

module.exports = connection;