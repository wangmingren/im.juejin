-- 创建数据库
create database juejin;
-- 使用数据库
use juejin;
-- 创建注册表
create table zhuce(
    zhu_id int primary key auto_increment comment '注册用户id',
    zhu_username varchar(255) not null comment'注册用户名',
    zhu_password varchar(255) not null comment'注册密码',
    zhu_phonum varchar(255) not null comment'注册手机号',
);
-- 插入数据
insert into zhuce values
(null,'王明仁','123456','17681358594','123456@qq.com'),
(null,'朱碧莹','654321','12320569704','654321@qq.com'),
(null,'贾炅辉','111111','13389259491','111111@qq.com');
-- 创建个人资料表
create table datum (
    datum_id int primary key auto_increment comment '注册用户id',
    dat_avatar varchar(255) default '/uploads/avatar_1.jpg',
    dat_name varchar(255) not null comment '用户名',
    dat_profession varchar(255) not null comment '职位',
    dat_firm varchar(255) not null comment '公司',
    dat_self varchar(255) not null comment '个人介绍',
    dat_homepage varchar(255) not null comment '个人主页'
);
insert into datum values 
(null,default,'王明仁','学生','江苏','全栈三四','气温仍然士大夫士大夫'),
(null,default,'王明仁','学生','江苏','全栈三四','气温仍然士大夫士大夫'),
(null,default,'王明仁','学生','江苏','全栈三四','气温仍然士大夫士大夫');
alter table zhuce add userid varchar(255);
alter table datum add userid varchar(255);

alter table datum add setid varchar(255);
alter table point add setid varchar(255);

alter table datum add fontid varchar(255);
alter table topic add fontid varchar(255);

alter table datum add listid varchar(255);
alter table chure add listid varchar(255);

alter table datum add prrid varchar(255);
alter table atten add prrid varchar(255);
-- 沸点
create table point (
    point_id int primary key auto_increment comment '发布id',
    point_area text not null comment '用户发布内容'
);

insert into point values 
(null,'去去微软推一头热围绕太阳'),
(null,'是豆腐干风格和关怀计划规划');

alter table datum add setid varchar(255);
alter table point add setid varchar(255);
-- 话题
create table topic (
    topic_id int primary key auto_increment comment '话题id',
    topic_avatar varchar(255) default '/uploads/avatar_1.jpg',
    topic_name varchar(255) not null comment '图名',
    topic_atten varchar(255) not null comment '关注人数',
    topic_pic varchar(255) not null comment '热度沸点',
    topic_title varchar(255) not null comment '图片title'
);
insert into topic values 
(null,'./img/fll/icon-1.jpg','一图胜千言','4006','2587','能用图，就不要用字'),
(null,'./img/fll/icon-2.jpg','今天学到了','4035','2259','日拱一卒，功不唐捐。一起来分享下你今天听到的有意思的事情，或者学习的冷知识。'),
(null,'./img/fll/icon-3.jpg','开发工具推荐','6378','809','来推荐一些你常用的能帮你提高效率的开发工具，不限于 IDE，开发框架，构建工具'),
(null,'./img/fll/icon-4.jpg','应用安利','2640','780','分享好玩、高颜值、实用的 APP、插件、扩展、小程序、H5…记得附上他们的介绍和下载地址哟'),
(null,'./img/fll/icon-5.jpg','掘金相亲','4292','345','千里姻缘掘金牵，有情人终成眷属。
必须的信息：
- 个人信息：性别，坐标，工作；
- 对另一半的期待：
其他信息选填：年龄，身高体重，爱好，联系方式，照片
注意：话题下未按照格式或无关沸点会被移除话题'),
(null,'./img/fll/icon-6.jpg','今日最佳','1590','675','The best or nothing.'),
(null,'./img/fll/icon-7.jpg','内推招聘','6697','936','与大厂面对面，零距离。
发布格式：
JD + 公司环境配图
（话题下非相关性沸点会做折叠处理，比如只发一句话加链接）'),
(null,'./img/fll/icon-8.jpg','上班摸鱼','615','646','来分享下你上班看到的好东西吧'),
(null,'./img/fll/icon-9.jpg','提问回答','1213','640','#提问回答#，所提的问题要同技术、程序员有关，参考方向：技术前景、最优解、编程问题…，涉及编程开发的问题其描述需尽可能详细。一块通过#提问回答#茁壮成长吧~'),
(null,'./img/fll/icon-10.jpg','我的开源项目','4736','682','来沸点曝光你的优秀开源项目
投稿格式：
【开源项目名 - 开源项目介绍】
开源项目地址：https://juejin.im
介绍：是什么，能做什么，一些故事
加上1到3张配图。'),
(null,'./img/fll/icon-11.jpg','优秀开源推荐','1538','420','来推荐你觉得优秀的开源项目（项目介绍+项目地址），不限于开源框架、开发工具~'),
(null,'./img/fll/icon-12.jpg','人在职场','1290','371','人在职场哪能时时顺风顺水，来聊聊工作中开心与不开心的事'),
(null,'./img/fll/icon-13.jpg','New 资讯','415','1276','来分享你刚看到过的商业科技 / 互联网 / 科技数码资讯吧~'),
(null,'./img/fll/icon-14.jpg','AMA','783','23','AMA：Ask ME Anything ，掘金团队会邀请各领域技术大牛通过「你问我答」的形式回答你的问题，让大家在技术、工作、生活方面有所成长。'),
(null,'./img/fll/icon-15.jpg','划个知识点','679','362','分享下你刚学到的知识点，不仅限于代码笔记'),
(null,'./img/fll/icon-16.jpg','你怎么看？','1876','293','能如何看待如何看待…，对此，你怎么看？来这里分享下你想和掘金小伙伴讨论的问题吧~用图，就不要用字'),
(null,'./img/fll/icon-17.jpg','我 & 掘金','232','142','来晒晒掘金给你发的周边，还有你发生在掘金里的事情，文章阅读破百万，收获 1w 个粉丝啦~'),
(null,'./img/fll/icon-18.jpg','掘金官方','506','68','关注该话题你可以看到掘金官方消息'),
(null,'./img/fll/icon-19.jpg','这个设计有点优秀','1018','276','来分享你生活、工作中看到的好看、另类设计~'),
(null,'./img/fll/icon-20.jpg','下班打卡','1385','347','下班的同学来打个卡，聊聊今天过得怎么样'),
(null,'./img/fll/icon-21.jpg','读书笔记','3281','399','读不在三更五鼓，功只怕一曝十寒。'),
(null,'./img/fll/icon-22.jpg','树洞一下','429','261','来分享下你的开心和不开心，此话题下内容统一由官方机器人账号代发，本话题只讲故事'),
(null,'./img/fll/icon-23.jpg','程序员鼓励师','227','161','来分享下治愈、鼓励你的图片，不只是那些喵星人、汪星人、兔星人、鼠星人… Gakki 亦可 (≧▽≦)'),
(null,'./img/fll/icon-24.jpg','薅羊毛','2727','133','走过路过不要错过，软件降价通知，App Store 限免，能薅的都在这里'),
(null,'./img/fll/icon-25.jpg','代码写诗','1628','131','Talk is cheap, show me your code。用优雅的代码书写美丽诗篇，建议用在线代码图片 Carbon 生成图片，Carbon 链接：https://carbon.now.sh/'),
(null,'./img/fll/icon-26.jpg','定个小目标','331','179','先定个小目标，完成了记得评论个 Done~'),
(null,'./img/fll/icon-27.jpg','朴素一餐','1032','177','分享一下咱程序员每天都吃了啥。'),
(null,'./img/fll/icon-28.jpg','好文推荐','520','144','在这里分享你发现的优质掘金文章 & 其他平台文章，可以自荐，所有沸点在发布后 24 小时没能获得 3 个点赞将会从话题下移除，但原内容保留(≧▽≦)'),
(null,'./img/fll/icon-29.jpg','掘金前端小组','1111','127','本话题主理人为：@yck
在这个话题下：
1. 分享自己不知道如何解决的前端问题
2. 分享前端的小知识点
3. 分享前端面试经历与面试题
4. 分享前端最新的技术动态，比如 Vue、React 框架动态
5. 分享有趣好玩的前端梗'),
(null,'./img/fll/icon-30.jpg','年中总结','919','87','上半年结束了，年初的小目标完成的怎么样啦？
参考格式：
- 用 3 个关键词概括上半年，eg：脱单、跳槽、加薪、star 过 1k…
- 展开描述上半年总结'),
(null,'./img/fll/icon-31.jpg','每天一道算法题','7188','104','每天精选一道算法题，帮助你更好地精进。'),
(null,'./img/fll/icon-32.jpg','求职中','543','66','在这里发布你的求职信息，包括：
必要：工作年限、求职领域、会的技能点；
选填：项目经历、年龄、教育背景；
（话题下不符合话题要求的沸点将被移除，如果找到满意的工作记得删掉求职沸点哟~~）'),
(null,'./img/fll/icon-33.jpg','代码秀','1598','66','不论语言，不计行数，show show 你的代码片段。建议用在线代码图片 Carbon 生成图片，Carbon 链接：https://carbon.now.sh/。'),
(null,'./img/fll/icon-34.jpg','Google IO','2166','78','2018年 Google IO 就要来了，你感觉又会发布哪些新技术，哪些新的技术方向？'),
(null,'./img/fll/icon-35.jpg','什么值得买','801','93','一起来分享你买过的好东西吧~'),
(null,'./img/fll/icon-36.jpg','反馈 & 建议','1146','173','大家可以在这里给掘金提需求、反馈 bug，每一条内容我们都会看，掘金的产品、设计、开发也会经常和大家互动。'),
(null,'./img/fll/icon-37.jpg','WWDC','1443','47','WWDC - 苹果全球开发者大会，是苹果公司每年定期在美国加州举办的会议活动，主要目的是向该公司产品的相关软件的设计师们展示最新的软件和技术。'),
(null,'./img/fll/icon-38.jpg','年终总结','358','63','一年又要结束了，年初的小目标完成的怎么样啦？
参考格式：
- 用 1 个句话概括即将过去的 2018 年，eg：GitHub 绿了 365 天…或者用 3 个关键词概括一年，eg：#脱单#、#加薪#、#离职#…
- 展开描述一年总结'),
(null,'./img/fll/icon-39.jpg','谷歌开发者大会','1300','75','谷歌开发者大会（Google Developer Days，简称 GDD）是展示 Google 最新开发者产品和平台的全球盛会。你可以在这里发布、了解最新的大会信息~'),
(null,'./img/fll/icon-40.jpg','掘金开发者大会','976','32','掘金开发者大会 · 微信小程序专场将于9月16日在北京富力万丽酒店举行，目前早鸟票在限时优惠中~'),
(null,'./img/fll/icon-41.jpg','LC3-2018','463','77','LC3 会议覆盖了开源界讨论最多、研究范围最广、实践探索最前沿的热点话题。'),
(null,'./img/fll/icon-42.jpg','进击的 React','643','34','本话题主理人为：程墨
在这个话题下，你可以进行 React 技术及前端开发的前沿讨论与资讯分享。'),
(null,'./img/fll/icon-43.jpg','报 offer','16','6','沸点#报offer#活动 ing http://t.cn/EKogyRc，该话题的沸点需要有以下信息：
- 必有项：
- 个人信息（工作年限 & 会的技术栈）
- 公司信息（公司名称）
- 岗位信息（岗位名称）
- 拿到 offer 时间
- 可选项：
- 薪资信息，例如：20k - 30k/月，10w - 20w/年
- 面试（过程&心得）');
-- 分享
create table share (
    share_id int primary key auto_increment comment '分享id',
    share_url varchar(255) not null comment '分享网站',
    share_tit varchar(255) not null comment '分享标题',
    share_text varchar(255) not null comment '分享描述',
    share_tag varchar(255) not null comment '分享标签',
    share_avatar varchar(255) default '/uploads/avatar_1.jpg'
) charset utf8;
set names gbk;
insert into share values 
(null,'https://www.baidu.com','hello','你好','全栈',default),
(null,'https://www.baidu.com','hi','哈罗','java',default),
(null,'https://www.baidu.com','hello','啊你哈噻有','后端',default);
-- 小册
create table chure (
    chure_id int primary key auto_increment comment "小册id",
    chure_name varchar(255) not null comment '小册名字',
    chure_text varchar(255) not null comment '小册内容',
    chure_nick varchar(255) not null comment '小册作者名',
    chure_buy varchar(255) not null comment '小册价格',
    chure_self varchar(255) not null comment '小册人数'
);
insert into chure values 
(null,'怎样怎样怎样','去微软推哦怕','啦啦啦','123','2222'),
(null,'怎样怎样怎样','去微软推哦怕','啦啦啦','123','2222'),
(null,'怎样怎样怎样','去微软推哦怕','啦啦啦','123','2222');
-- 查看更多
create table look (
    look_id int primary key auto_increment comment '查看id',
    look_name varchar(255) not null comment '查看姓名',
    look_area varchar(255) not null comment '查看内容'
);
insert into look values 
(null,'小生方琴','coder'),
(null,'纯粹的的','vovov'),
(null,'阿三顶顶','dfsfffs');
-- 文章
create table book (
    book_id int primary key auto_increment comment '文章id',
    book_name varchar(255) not null comment '姓名',
    book_tit varchar(255) not null comment '用户标题',
    book_area varchar(255) not null comment '用户内容',
    book_lab varchar(255) not null comment '标签',
    book_next varchar(255) not null  comment '选择标签'
);
insert into book values
(null,'王明仁','全栈','去微软','古hi依附于','fffr'),
(null,'王明','全','去软','古依附于','ffr'),
(null,'王明仁','全栈','去微软','古hi依附于','fffr'),
(null,'王明','全','去软','古依附于','ffr'),
(null,'王明','全','去软','古依附于','ffr'),
(null,'王明','全','去软','古依附于','ffr'),
(null,'王仁','栈','去微','古h依附于','ff');
-- 活动
create table active (
    active_id int primary key auto_increment comment '活动id',
    active_avatar varchar(255) default './img/jjh/lubbo-2.jpg',
    active_title varchar(255) not null comment '活动标题',
    active_tit varchar(255) not null comment '活动小标题',
    active_address varchar(255) not null comment '位置'
);
insert into active values 
(null,default,'亚的风格和规范v规划报告v发','ASDDS','杭州'),
(null,default,'亚的风格和规范v规划报告v发','ASDDS','杭州'),
(null,default,'亚的风格和规范v规划报告v发','ASDDS','杭州'),
(null,default,'亚的风格和规范v规划报告v发','ASDDS','杭州'),
(null,default,'亚的风格和规范v规划报告v发','ASDDS','杭州');
-- 关注
create table atten (
    atten_id int primary key auto_increment comment '关注id',
    atten_name varchar(255) not null comment '关注姓名',
    atten_data varchar(255) not null comment '关注时间',
    atten_tit varchar(255) not null comment '关注小标题',
    atten_nickname varchar(255) not null comment '关注别名'
);
insert into atten values 
(null,'王明仁','学生 · 11小时前','全栈','不爱吃葡萄干'),
(null,'王明仁','学生 · 11小时前','全栈','不爱吃葡萄干'),
(null,'王明仁','学生 · 11小时前','全栈','不爱吃葡萄干'),
(null,'王明仁','学生 · 11小时前','全栈','不爱吃葡萄干'),
(null,'王明仁','学生 · 11小时前','全栈','不爱吃葡萄干');
