// 引入mysql包
const mysql = require('mysql');

// 链接数据库
const connection = mysql.createConnection({
  // 数据库地址
  host: '127.0.0.1',
  // 端口
  port: '3306',
  // 数据库用户名
  user: 'root',
  // 数据库密码
  password: 'root',
  // 数据库名称
  database: 'juejin'
})

// 暴露数据库
module.exports = connection;