let show = function () {
  $.ajax({
    type: 'get',
    url: '/showBoiling',
    data: '',
    success: function (data) {
      // console.log(data);
      // 配置模板
      let tempData = template('boilingTemp', { list: data });
      // 把内容写进publis-nav
      $('.publis-nav').html(tempData);
      $('.usernav').css('marginBottom', '15px')
    }
  })
}

// 1.发布内容
// 给发布按钮添加点击事件
$('.issue-btn').on('click', function () {
    // 获取textarea的内容
    let textData = $('.textbox').serialize();
    // console.log(textData);
    // 发起ajax请求
    $.ajax({
      type: 'post',
      url: '/addBoiling',
      data: textData,
      success: function (data) {
        // console.log(data);
        if (data.code == '1000') {
          alert(data.msg);
          // 清空 发布框的值
          $('.textbox').val('');
        }
      }
    })
})

// 2. 显示发布内容
  $('.issue-btn').on('click', function () {
    $.ajax({
      type: 'get',
      url: '/showBoiling',
      data: '',
      success: function (data) {
        // console.log(data);
        // 配置模板
        let tempData = template('boilingTemp', { list: data });
        // 把内容写进publis-nav
        $('.publis-nav').html(tempData);
        $('.usernav').css('marginBottom', '15px')
      }
    })
  })

// 3. 删除发布内容
// 给删除按钮添加点击事件
$('.publis-nav').on('click', '.frame', function () {
  // 获取内容id
  let textId = $(this).data('id');
  // console.log(textId);
  $.ajax({
    type: 'get',
    url: '/delBoiling',
    data: {id: textId},
    beforeSend: function () {
      let flag = confirm('你确定要删除吗？');
      if (!flag) {
        return false;
      }
    },
    success: function (data) {
      // console.log(data);
      if (data.code == '1001') {
        alert('删除成功');
        // 重新渲染查询页面
        show();
      }
    }
  })
})
