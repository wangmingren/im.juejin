// 添加数据到数据库
// 上传图片
$('#feature').on('change',function () {
  // 获取图片里的表单信息
  let file = $(this)[0].files[0];
  // 实例化formDate
  let formdata = new FormData();
  // 把图片里的表单信息追加到 formdata里 ('name属性'，图片信息)
  formdata.append('feature',file);
  // 发起ajax请求
  $.ajax({
    type: 'post',
    url: '/addFile',
    data: formdata,
    contentType: false,
    processData: false,
    success: function (data) {
      // console.log(data);
      $('.file-son img').show().attr('src',data.picAdd).after('<input type="hidden" name="feature" value="'+data.picAdd+'">');
    }
  })
})

// 发布
$('.pub-btn').on('click',function () {
  // 获取信息
  let pubData = $('.form-name').serialize();
  // console.log(pubData);
  $.ajax({
    type: 'post',
    url: '/pubShare',
    data: pubData,
    success: function (data) {
      // console.log(data);
      if (data.code == '1100') {
        alert(data.msg);
        location.href = '/share-link';
      }
    }
  })
})