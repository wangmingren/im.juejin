// 获取 小导航栏
var tab = document.querySelector('.top-nav');
// 获取隐藏栏
var nav = document.querySelector('.top-nav-none');
// 获取 关注框
var atten = document.querySelector('attention');

// 固定右边导航栏
$(window).scroll(function () {
    // 如果滚动高度大于 230，让隐藏栏显示，并固定
    if (ScollPostion().top > 230) {
        nav.style.display = "inline-block";
        nav.style.position = "fixed";
        // atten.style.position = 'fixed';
    } else {
        // 否则隐藏
        nav.style.display = "none";
    }

    if (ScollPostion().top > 320) {
        tab.style.display = "none";
        nav.style.marginTop = -77 + 'px';
    } else {
        tab.style.display = "inline-block";
        nav.style.marginTop = 0 + 'px';
    }
})

function ScollPostion() { //滚动条位置
    var t, l, w, h;
    if (document.documentElement && document.documentElement.scrollTop) {
        t = document.documentElement.scrollTop;
        l = document.documentElement.scrollLeft;
        w = document.documentElement.scrollWidth;
        h = document.documentElement.scrollHeight;
    } else if (document.body) {
        t = document.body.scrollTop;
        l = document.body.scrollLeft;
        w = document.body.scrollWidth;
        h = document.body.scrollHeight;
    }
    return {
        top: t,
        left: l,
        width: w,
        height: h
    };
}

// table切换
// 获取所有标题
var aBtn = document.querySelectorAll('.table li a');
// 循环每一个a标签
for (var i = 0; i < aBtn.length; i++) {
    aBtn[i].onclick = function () {
        // 获取active的a标签
        var activeA = document.querySelector('.active');
        // 获取a标签的自定义属性
        var con = activeA.getAttribute('cont');
        // 获取内容盒子
        var mainC = document.querySelector('.main-cont');
        // 通过上面获取的值，找到相对应的内容版心 让他隐藏
        document.getElementById(con).style.display = 'none';
        // 移除active
        activeA.removeAttribute('class');
        // 给点击的a标签设置class为active
        this.className = 'active';
        // 获取点击a标签的cont值
        var cont = this.getAttribute('cont');
        // 根据cont属性的值获取对应的内容让其显示
        document.getElementById(cont).style.display = 'block';
        mainC.style.position = 'absolute';
        // mainC.style.marginTop = 110 + 'px';
        this.style.borderBottom = '1+"px" "solid" "#007fff"';
        return false;
    }
}