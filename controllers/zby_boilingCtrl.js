// 0. 引入数据库
const conn = require('../data/zby_boilingData');

// 1.显示 沸点页面
module.exports.boiling = (req, res) => {
  res.render('./zby/boiling');
}

// 2.发布内容
module.exports.addBoiling = (req, res) => {
  let pointStr = [req.body.point_area];
  // console.log(pointStr);
  // 使用mysql的query方法
  conn.query('insert into point values(null,?)',req.body.point_area,(error, results) => {
    // 如果错误，打印错误信息
    if (error) {
      console.log(error);
    } else {
      // console.log(results);
      // 否则发送数据到前端
      res.json({
        code: '1000',
        msg: '发布成功'
      })
    }
  })
}

// 显示发布内容
// 3.显示发布内容
module.exports.showBoiling = (req, res) => {
  conn.query('select * from point order by point_id desc',(error, results) => {
    if (error) {
      console.log(error);
    } else {
      // console.log(results);
      res.json(results);
    }
  })
}

// 4. 删除发布内容
module.exports.delBoiling = (req, res) => {
  conn.query('delete from point where point_id =?',req.query.id,(error, results) => {
    if (error) {
      console.log(error);
    }
    if (results.affectedRows) {
      res.json({
        code: '1001',
        msg: '删除成功'
      })
    }
  })
}


