// 引入数据库
const conn = require('../data/zby_boilingData');
// 引入fs
const fs = require('fs');

// 1. 显示分享链接
module.exports.share = (req, res) => {
  res.render('./zby/share-link');
}

// 2. 上传图片
module.exports.addFile = (req, res) => {
  // console.log(req.file);
  fs.writeFile('./public/uploads/' + req.file.originalname,req.file.buffer,(error,results) => {
    if (error) {
      console.log(error);
    } else {
      res.json({
        picAdd: './uploads/' + req.file.originalname
      })
    }
  })
}

// 3. 发布数据
module.exports.pubShare = (req, res) => {
  let shareStr = [req.body.share_url,req.body.share_tit,req.body.share_text,req.body.share_tag,req.body.feature];
  conn.query('insert into share(share_url,share_tit,share_text,share_tag,share_avatar) values(?,?,?,?,?)',shareStr,(error, results) => {
    if (error) {
      console.log(error);
    } else {
      // console.log(results);
      res.json({
        code: '1100',
        msg: '发布成功'
      })
    }
  })
}