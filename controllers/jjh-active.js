const conn = require('../data/conn');

module.exports.active = (req, res) => {
    res.render('./jjh/active');
}

module.exports.activeFind = (req, res) => {
    conn.query('select * from active', (err, result) => {
        if (err) return console.log(err);

        res.json(result);
    })
}