const conn = require('../data/conn');
// 返回登陆页面
module.exports.login = (req, res) => {
    res.render('./zbx/login');
}
module.exports.zhuce = (req, res) => {
    res.render('./zbx/zhuce')
}
module.exports.userAdd = (req, res) => {
    // console.log(req.body);
    let sql = 'insert into zhuce(zhu_username,zhu_phonum,zhu_password) values(?,?,?)';
    let data = [req.body.registerUsername, req.body.registerPhoneNumber, req.body.registerPassword];
    conn.query(sql, data, (eer, result) => {
        if (eer) return console.log(eer);
        res.json({
            code: '10002',
            message: '添加成功'
        })
    })
}

module.exports.reset = (req, res) => {
    // console.log(req.body);
    let sql = 'select * from zhuce where zhu_phonum=? and zhu_password=?';
    let datas = [req.body.zhu_phonum, req.body.zhu_password];
    conn.query(sql, datas, (eer, results) => {
        if (eer) return console.log(eer);
        // console.log(results.length);
        // console.log(results.length);

        if (results.length == 0) {
            res.json({
                code: '1002',
                message: '密码或者账号错误'
            })
        } else if (results.length != 0) {
            req.session.iswangren = true;
            req.session.users = results[0];
            res.json({
                code: '1003',
                message: '登录成功'
            })
        }
    })

}