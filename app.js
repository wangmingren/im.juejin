// 引包
const express = require('express');
const session = require('express-session');
// 创建服务器
const app = express();
app.use(session({
    secret: 'wangren',
    resave: false,
    saveUninitialized: false
}))
// 配置body parser 
const bodyparser = require('body-parser');
app.use(bodyparser.urlencoded({ extended: false }));
// 配置静态目录
app.use(express.static('./public'));
// 配置ejs模板



// 配置tempalte模板
// app.engine('html',require('express-art-template'));
// app.set('views','./views');
// 引入路由
const loginRouter = require('./routes/zbx_Router');
app.use(loginRouter);
const indexRoute = require('./routes/indexRoute');
app.use(indexRoute);
const allroute = require('./routes/all_topics');
app.use(allroute);

// 活动
const activeRoute = require('./routes/jjh-active');
app.use(activeRoute);

// 合作
const hezuoRoute = require('./routes/jjh-hezuo');
app.use(hezuoRoute);

// 文件
const wenjianRoute = require('./routes/jjh-wenjian');
app.use(wenjianRoute);

// 配置ejs模板
const ejs = require('ejs');
app.set('view engine', 'ejs');

// 引入 沸点路由
const boilingRouter = require('./routes/zby_boilingRouter');
// 挂载 沸点路由
app.use(boilingRouter);

// 引入 分享链接路由
const shareRouter = require('./routes/zby_shareRouter');
// 挂载 分享链接路由
app.use(shareRouter);


// 监听事件
app.listen(7788, () => {
    console.log("http://127.0.0.1:7788");
});
